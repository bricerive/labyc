# create a MacOSX droplet with icon and config file
MACRO( PlatypusIt Target )
	if (APPLE)
		# Locate the platypus command-line executable
		find_program(PlatypusPath platypus)
		
		# Generate a little script to make the glue between platypus renaming the application: 'script',
		# and our application wanting its name untouched
		configure_file(${CMAKE_CURRENT_SOURCE_DIR}/platypus.sh.in
			${CMAKE_CURRENT_BINARY_DIR}/${Target}.sh
			@ONLY
		)
		
		# Parse each extra parameter as an option value or an extra resource file
		set( OutputMode -o 'Text Window' )
		set( DropMode -D -X '*' )
		set( LingerMode )
		foreach( PlatypusOption ${ARGN} )
			if ( ${PlatypusOption} STREQUAL "NO_CONSOLE" )
				set( OutputMode -o None -R )
			elseif ( ${PlatypusOption} STREQUAL "NO_DROP" )
				set( DropMode )
			elseif ( ${PlatypusOption} STREQUAL "NO_LINGER" )
				set( LingerMode -R)
			else ()
				set( StagedFileOption ${StagedFileOption} -f ${PlatypusOption} )
			endif()
		endforeach()
		
		# Generate the platypus call
		add_custom_command(TARGET ${Target}
			POST_BUILD
			COMMAND ${PlatypusPath}
			ARGS
				${OutputMode} ${DropMode} ${LingerMode} -y
				-i ${CMAKE_CURRENT_SOURCE_DIR}/${Target}.icns
				-f $<TARGET_FILE:${Target}>
				${StagedFileOption}
				${CMAKE_CURRENT_BINARY_DIR}/${Target}.sh ${Target}.app
			WORKING_DIRECTORY ${StageDir}
			COMMENT Running Platypus for ${Target}
			)
   	endif()
ENDMACRO()
