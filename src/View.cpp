// File Name: View.cc
#include <math.h>
#include "View.h"
#include "labyConfigAccess.h"
#include "mylib/config.h"

#define FVALPI       (3.14159265)
#define FVAL2PI      (6.28318531)
#define FVALHALF_PI  (1.57079633)
#define FVAL3HALF_PI  (4.71238897)
#define FVALQUART_PI  (0.78539816)
#define FVAL3QUART_PI  (2.35619448)
#define FVAL5QUART_PI  (3.92699081)
#define FVAL7QUART_PI  (5.49778713)

bool View::ChangeView(Intent &intent, World */*world*/)
{
    CONFIG_VAR(double,  LockSpeed);
    CONFIG_VAR(double,  Inertia);
    int i;
    bool halt=false, lock=false;
    double minDelta = .001;

    static int locking=0;
    static double lockDx, lockDy, lockDa;
    static double dest_x, dest_y, dest_a;

    /* Check for special intents */
    for (i=0; i<intent.n_special; i++) {
	if (intent.special[i] == INTENT_HALT) halt = true;
	if (intent.special[i] == INTENT_LOCK) lock = true;
    }
    
    /* Compute locking destination */
    if (lock && !locking) {

	if (x >= 0)
	    dest_x = 5 + 10 * (int)(x/10);
	else
	    dest_x = -5 - 10 * (int)(-x/10);
	if (y >= 0)
	    dest_y = 5 + 10 * (int)(y/10);
	else
	    dest_y = -5 - 10 * (int)(-y/10);

	if (a < FVALQUART_PI)
	    dest_a = 0.0;
	else if(a < FVAL3QUART_PI)
	    dest_a = FVALHALF_PI;
	else if(a < FVAL5QUART_PI)
	    dest_a = FVALPI;
	else if(a < FVAL7QUART_PI)
	    dest_a = FVAL3HALF_PI;
	else
	    dest_a = FVAL2PI;

	locking = LockSpeed;
	lockDx = (dest_x-x)/locking;
	lockDy = (dest_y-y)/locking;
	lockDa = (dest_a-a)/locking;

    }

    /* Disable locking if necessary */
    if (locking &&
	(intent.force_x || intent.force_y || intent.force_rotate || halt)) {
	locking=0;
	dx = 0.0;
	dy = 0.0;
	da = 0.0;
    }

    if (halt) {
	dx = dy = da = 0.0;
    } else if (locking) {
	x += lockDx;
	y += lockDy;
	a += lockDa;
	as = sin(a);
	ac = cos(a);
	if (--locking == 0) {
	    x = dest_x;
	    y = dest_y;
	    a = dest_a;
	    dx = 0.0;
	    dy = 0.0;
	    da = 0.0;
	}
    } else {
	dx += (intent.force_x - dx)/Inertia;
	dy += (intent.force_y - dy)/Inertia;
	da += (intent.force_rotate - da)/Inertia;

	/* Clip low */
	if ((dx > 0.0 && dx < minDelta) ||
	    (dx < 0.0 && dx > -minDelta))
	    dx = 0.0;
	if ((dy > 0.0 && dy < minDelta) ||
	    (dy < 0.0 && dy > -minDelta))
	    dy = 0.0;
	if ((da > 0.0 && da < minDelta) ||
	    (da < 0.0 && da > -minDelta))
	    da = 0.0;

	/* Compute new angle */
	a += 0.3 * da;
	if (a >= FVAL2PI) a -= FVAL2PI;
	if (a < 0.0) a += FVAL2PI;
	as = sin(a);
	ac = cos(a);
    
	/* Compute new position */
	x += 0.8 * dx * ac;
	y += 0.8 * dx * as;
	x += 0.8 * dy *-as;
	y += 0.8 * dy * ac;
    }
    
    return false;
}

View::~View()
{
}

View::View()
{
    CONFIG_VAR(int,  ViewPlaneSize);
    CONFIG_VAR(int,  ViewEyeDist);
    CONFIG_VAR(int,  FrameWidth);
    CONFIG_VAR(int,  FrameHeight);

    viewPlaneSize = ViewPlaneSize;
    eyeDistance = ViewEyeDist;
    x=y=dx=dy=da=0.0;
    a=0.0; as=0.0; ac=1.0; // along x
    a=3.14159/4.0;as=ac=sqrt(2.0)/2.0; // 45 deg
    pixDx = 2*viewPlaneSize/FrameWidth;
    pixDy = 2*viewPlaneSize/FrameHeight;
}

/*********************** End Of View.cc ***************************/
