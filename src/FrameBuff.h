// FrameBuff.h -- interface for FrameBuff class
#ifndef FrameBuff_h
#define FrameBuff_h

typedef unsigned int PIX;

class FrameBuff {
 public:
    FrameBuff(int height, int width);
    virtual ~FrameBuff();
    PIX *ChangeBuff(PIX *buff);
    PIX *PixP()const {return(pixP);}
    int Height()const {return height;}
    int Width()const {return width;}
    
 private:
    int height;
    int width;
    PIX *pixP;
};

#endif
/*************************** End FrameBuff.h ************************/
