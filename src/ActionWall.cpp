// File Name: ActionWall.c
#include "ActionWall.h"

// Update a wall to current view
bool ActionWall::Update(View */*view*/)
{
    return false;
}

// User tried to lock on wall
bool ActionWall::Lock(View */*view*/)
{
    return false;
}

// User tried to unlock from wall
bool ActionWall::Unlock(View */*view*/)
{
    return false;
}

ActionWall::~ActionWall()
{
}

ActionWall::ActionWall(Vertex *v1, Vertex *v2, Texture *t)
	:Wall(v1,v2,t)
{
}

/*********************** End Of ActionWall.cc ***************************/
