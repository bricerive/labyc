// ActionWall.h -- interface for actionWall class
#ifndef ActionWall_h
#define ActionWall_h

#include "Wall.h"
#include "View.h"

class ActionWall:public Wall {
 public:
    ActionWall(Vertex *v1, Vertex *v2, Texture *t);
    virtual ~ActionWall();
    bool Update(View *view);
    bool Lock(View *view);
    bool Unlock(View *view);
};
#endif
/*************************** End ActionWall.h ************************/
