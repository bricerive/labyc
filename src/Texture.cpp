/*
 * File Name: Texture.cc
 */
#include <string.h>
#include <iostream>
using namespace std;

#include "Texture.h"
#include "labyConfigAccess.h"
#include "mylib/error.h"
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

using namespace mylib;

#define GRAY(v) (0xFF|(v<<8)|(v<<16)|(v<<24))
#define COLOR(v,c) 0xFF | ((((c>>8)&0xFF)*v/255)<<8) | ((((c>>16)&0xFF)*v/255)<<16) | ((((c>>24)&0xFF)*v/255)<<24)

bool Texture::Generate(int code)
{
    PIX v;
    PIX color = (rand()&0xFF)|((rand()&0xFF)<<8)|((rand()&0xFF)<<16)|((rand()&0xFF)<<24);
	unsigned char m=255;
	
    for (int i=0; i<dim; i++)
		for (int j=0; j<dim; j++) {
		    switch(code%7) {
                case 0:
                    v = (i&16)^(j&16)? 0: m; // Checker board
                    break;
                case 1:
                    v = (i&4)? 0: m; // Horizontal lines
                    break;
                case 2:
                    v = (j&16||i&16)? m: 0; // Thick horizontal & Vertical lines
                    break;
                case 3:
                    v = (j&4||i&4)? m: 0; // Thin horizontal & Vertical lines
                    break;
                case 4:
                    v = (j&4)? 0: (i/4); // Decaying columns
                    break;
                case 5:
                    v = (i&4)? 0: (j/4); // Decaying lines
                    break;
                case 6:
                    v = rand()&0xFF; // Random noise
                    break;
		    }
			//TexP(i,j) = GRAY(v);
			TexP(i,j) = COLOR(v,color);
		}
    FillLast();
    return false;
}

void Texture::FillLast() {
    static const PIX p1 = 0x80C04040; // ABGR
    static const PIX p2 = 0x804040C0; // ABGR
    for (int j=0; j<dim; j++) TexP(dim,j) = p1;
    for (int j=0; j<dim; j++) TexP(dim+1,j) = p2;
}

// Load a texture from a file
bool Texture::Load(const char *fileName)
{	
    sf::Image image;
    if (!image.loadFromFile(fileName))
    {
        printf("Could not open texture file %s\n", fileName);
        return true;
    }
	// Load image
	const unsigned char *img=image.getPixelsPtr();
    sf::Vector2u sz = image.getSize();
	int width=sz.x,height=sz.y;
	
	// Scale the image
	const unsigned char *src;
	unsigned char *dst;
	int sum[4];
	float scale = max((float)width/dim, (float)height/dim);
	int scaleI = (int)scale;
    static const PIX blackPix = 0x804040C0;
	const unsigned char *black=reinterpret_cast<const unsigned char *>(&blackPix);
	for (int i=0; i<dim; i++) {
	    for (int j=0; j<dim; j++) {
			sum[0]=sum[1]=sum[2]=sum[3]=0;
			for (int k=0; k<scaleI; k++)
			    for (int l=0; l<scaleI; l++) {
			    	int dy = (int)(height/2 - dim/2*scale + i*scale + k);
			    	int dx = (int)(width/2 - dim/2*scale + j*scale + l);
			    	if (dx<0 || dx>=width || dy<0 || dy>=height)
						src = black;
				    else
				    	src = img + 4 *(width*dy + dx);
			    	sum[0] += src[0];
			    	sum[1] += src[1];
			    	sum[2] += src[2];
			    	sum[3] += src[3];
				}
			dst = (unsigned char *)(texP + dim * i + j);
			dst[0] = sum[0] / (scaleI*scaleI);
 			dst[1] = sum[1] / (scaleI*scaleI);
 			dst[2] = sum[2] / (scaleI*scaleI);
 			dst[3] = sum[3] / (scaleI*scaleI);
	    }
	}
    
    FillLast();
    return false;
}

Texture::~Texture()
{
    delete texP;
}

Texture::Texture()
{
    CONFIG_VAR(int, TextureDim);
    if (TextureDim >= 255)
        throw Err(_WHERE, "Texture too high %d>=255)", TextureDim);
    dim = TextureDim;
    texP = new PIX[dim*(dim+2)];
}

PIX *Texture::Point(double t)
{
    PIX *pixP;
    
    pixP = texP + (int)(t * (dim-1));
    return pixP;
}


/*********************** End Of Texture.cc ***************************/
