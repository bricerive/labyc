/*
 * File Name: Wall.cc
 */

#include "Wall.h"
#include "labyConfigAccess.h"
#include <math.h>

Wall::~Wall()
{
}

Wall::Wall(Vertex *v1, Vertex *v2, Texture *t)
{
    vertex1 = v1;
    vertex2 = v2;
    texture = t;
}

void Wall::ClipAndSort(View *view, FrontWall *frontWalls)
{
    CONFIG_VAR(int, FrameWidth);
    
    double vd = view->EyeDistance();
    double vp = view->ViewPlaneSize();
    double x1, y1, px1, x2, y2, px2, dx, dy;
    double a,b,c;
    unsigned int outcode1, outcode2;
    int fb1, fb2, fb;
    double z1, z2, z, dz=0;
    
    x1 = vertex1->TX();
    x2 = vertex2->TX();

    // Check if the wall is behind the view plane
    if (x1 < vd && x2 < vd)
		return;

    y1 = vertex1->TY();
    y2 = vertex2->TY();

    // Check that the wall is facing us
    if (x1*y2 >= y1*x2)
		return;

    // Compute wall line equation  ax+by+c=0
    a = y2-y1;
    b = x1-x2;
    c = x2*y1 - x1*y2;

    // Compute wall line length
    dx = fabs(b);
    dy = fabs(a);

    px1 = vertex1->Proj();
    px2 = vertex2->Proj();

    // Clip to the view line:
	//  xClip = eyeDist
    //  yClip = y + (eyeDist - x) * (y2-y1)/(x2-x1)
    if (x1 < vd) {
		// Clip left end
		if (-b < .000000001)
		    return;
		y1 = y1 + ((vd - x1)*a)/(-b);
		px1 = y1;
		x1 = vd;
    }
    if (x2 < vd) {
		// Clip right end
		if (b < .000000001)
		    return;
		y2 = y2 + ((vd - x2)*a)/(-b);
		px2 = y2;
		x2 = vd;
    }

    // clip to the sides of the view polygon.
    outcode1 = std::signbit(vp + px1);
    outcode1 |= std::signbit(vp - px1) << 1;
    outcode2 = std::signbit(vp + px2);
    outcode2 |= std::signbit(vp - px2) << 1;
    
    // Check if the wall is completely on either side
    if ((outcode1 & outcode2) != 0)
		return;
    
    if (outcode1 | outcode2) {
		double d,e,dy;
		
		d = b*vp;
		e = -c;
		dy = vp/vd;
		if (outcode1 & 2) {
		    // intersection with left screen border
		    x1 = e/(a + d);
		    y1 = x1*dy;
		    px1 = vp;
		}
		if (outcode2 & 1) {
		    // intersection with right screen border
		    x2 = e/(a - d);
		    y2 = -(x2*dy);
		    px2 = -vp;
		}
    }	
    
    // Convert projected coords into frame buffer coords
    px1 = px1/view->PixDX();
    px2 = px2/view->PixDX();
    fb1 = ((int)FrameWidth >> 1) - (int)px1 - (std::signbit(px1)^1);
    if (fb1<0) fb1=0;
    fb2 = ((int)FrameWidth >> 1) - (int)px2 - (std::signbit(px2)^1);
    if (fb2>=FrameWidth) fb2=FrameWidth-1;

    // Compute values of linearly interpolable z variable
    z1 = vd/x1;
    z2 = vd/x2;
    if (fb2!=fb1)
	dz = (z2-z1)/(fb2-fb1);

    for (fb=fb1,z=z1; fb<=fb2; fb++,z+=dz) {
		if (z > frontWalls[fb].z) {
		    frontWalls[fb].z = z;
		    frontWalls[fb].wall = this;
		}
    }
}
    

// Calculate the value of the parameter t at the intersection
//   of the view ray and the wall.  t is 0 at the origin of
//   the wall, and 1 at the other endpoint.
// Return a pointer to the texture at that column.
PIX *Wall::RayIntersect(double Vx, double Vy)
{
	return texture->Point(RayIntersectRatio(Vx, Vy));
}

double Wall::RayIntersectRatio(double Vx, double Vy)
{
    double denominator, Nx, Ny, Wx, Wy;
    double tx1=vertex1->TX(), tx2=vertex2->TX();
    double ty1=vertex1->TY(), ty2=vertex2->TY();
    double t;
    
    // View vector
    Nx = -Vy;
    Ny = Vx;
    // Wall vector
    Wx = tx2 - tx1;
    Wy = ty2 - ty1;
    
    denominator = Nx*Wx + Ny*Wy; /* N dot W */
    if (denominator < -.000000001)
        t = (Nx*tx1 + Ny*ty1)/(-denominator);
    else if (denominator > .000000001)
        t = 1.0 - (Nx*tx1 + Ny*ty1)/(-denominator);
    else
        t = 0.0;
    if (t<0) t=0;
    if (t>1.0) t=1.0;
    return t;
}

void Wall::Animate(View */*view*/)
{
}

SlidingWall::SlidingWall(Vertex *v1, Vertex *v2, Texture *t)
	:Wall(v1,v2,t)
{
	dxMax = X2()-X1();
	dyMax = Y2()-Y1();
	dMax = sqrt(dxMax*dxMax+dyMax*dyMax);
	dd = -dMax/50;
}

void SlidingWall::Animate(View * /*view*/)
{
	double dx = X2()-X1();
	double dy = Y2()-Y1();
	double d = sqrt(dx*dx+dy*dy);
	d = d+dd;	
	if (d<.000001) {
		d=0;
		dd = dMax/50;
	} else if (d>dMax) {
		d = dMax;
		dd = -dMax/50;
	}
	X2(X1() + dxMax * d/dMax);
	Y2(Y1() + dyMax * d/dMax);
}

RotatingWall::RotatingWall(Vertex *v1, Vertex *v2, Texture *t)
	:Wall(v1,v2,t)
{
}

void RotatingWall::Animate(View * /*view*/)
{
	double a=0.02;
	double ac=cos(a);
	double as=sin(a);
	double xc = (X1()+X2())/2;
	double yc = (Y1()+Y2())/2;
	double xc1 = X1() - xc;
	double yc1 = Y1() - yc;
	double xr1 = xc1*ac+yc1*as;
	double yr1 = yc1*ac-xc1*as;
	double xc2 = X2() - xc;
	double yc2 = Y2() - yc;
	double xr2 = xc2*ac+yc2*as;
	double yr2 = yc2*ac-xc2*as;
	vertex1->SetPos(xc+xr1,yc+yr1);
	vertex2->SetPos(xc+xr2,yc+yr2);
}

AnimWall::AnimWall(Vertex *v1, Vertex *v2, Texture *t)
	:Wall(v1,v2,t)
{
	toggle=0;
}

void AnimWall::Animate(View * /*view*/)
{
	toggle++;
}

// Calculate the value of the parameter t at the intersection
//   of the view ray and the wall.  t is 0 at the origin of
//   the wall, and 1 at the other endpoint.
// Return a pointer to the texture at that column.
PIX *AnimWall::RayIntersect(double Vx, double Vy)
{
    double t = RayIntersectRatio(Vx, Vy);
	if (toggle&0x40) t=.5+t/2;
	else t=t/2;
	return texture->Point(t);
}


/*********************** End Of Wall.cc ***************************/
