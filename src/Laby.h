// Laby.h -- interface for Laby class
#ifndef Laby_h
#define Laby_h

#include "Timer.h"
#include "World.h"
#include "FrameBuff.h"
#include "Console.h"
#include "View.h"

#define FLAG_LABY_LAST FLAG_GENERIC_LAST

class Laby {
 private:
    World *world;
    FrameBuff *frameBuff;
    Console *console;
    View *view;
    Timer *timer;
 public:
    Laby(Console *console);
    virtual ~Laby();
    void Run();
    int IsDone(Intent &intent);
};

#endif
/*************************** End Laby.h ************************/
