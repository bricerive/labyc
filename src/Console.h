// Console.h -- interface for Console class

#ifndef Console_h
#define Console_h

#include "FrameBuff.h"

enum {
    INTENT_END_GAME=1,
    INTENT_HALT,
    INTENT_LOCK
    };

#define MAX_SPECIAL_INTENTIONS  20
typedef struct {
    double force_x, force_y, force_z;
    double force_rotate;
    int n_special;
    int special[MAX_SPECIAL_INTENTIONS];
} Intent;

class Console {
 public:
    Console();
    virtual ~Console();
    virtual bool ReadInput(Intent &intent)=0;
    virtual bool Update()=0;
    FrameBuff *GetFrameBuff()const;
 protected:
    FrameBuff *frameBuff;
 private:
    double force_x;
    double force_y;
    double force_rotate;
};

#endif
