// Timer.h -- interface for Timer class
#ifndef Timer_h
#define Timer_h

#define MAX_NB_STEPS 32

class Timer {
 private:
    float threshold;
    int nbSteps;
    double lastTime;
    double times[MAX_NB_STEPS+1];
    const char *steps[MAX_NB_STEPS+1];
 public:
    Timer();
    virtual ~Timer();
    void Start();
    void Step(const char *msg);
    float Report(const char *title);
};
#endif
/*************************** End Timer.h ************************/
