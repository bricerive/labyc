/*
 * File Name: FrameBuff.cc
 */
#include <iostream>
#include "FrameBuff.h"

PIX *FrameBuff::ChangeBuff(
    PIX *buff
)
{
    PIX *oldBuff = pixP;
    pixP = buff;
    return(oldBuff);
}

FrameBuff::~FrameBuff(
)
{
    delete pixP;
}

FrameBuff::FrameBuff(
    int height,
    int width
)
{
    /* Set attributes */
    if (width & 0x3) {
	//cout << "Framebuff width has to be a factor of 4\n";
	width = 4 * ((width+3)/4);
    }
    this->height = height;
    this->width = width;
    pixP = new PIX[width*height];
}

/*********************** End Of FrameBuff.cc ***************************/
