/*
 * File Name: Vertex.cc
 */

#include "Vertex.h"
#include "View.h"

Vertex::~Vertex()
{
}

void Vertex::Transform(View *view)
{
    double rx = x - view->X();
    double ry = y - view->Y();
    double vd = view->EyeDistance();
    
    tx = rx*view->Cos() + ry*view->Sin();
    ty = -rx*view->Sin() + ry*view->Cos();
    if (tx > vd)
		proj = (ty/tx)*vd;
}

void Vertex::Rotate(Vertex */*center*/, double /*a*/)
{
}

/*********************** End Of Vertex.cc ***************************/
