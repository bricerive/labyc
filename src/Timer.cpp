/*
 * File Name: Timer.cc
 */
#include <stdio.h>
#include <time.h>
#include <iostream>
#include "Timer.h"

static double fpTime();

Timer::Timer()
{
    threshold=0;
}

Timer::~Timer()
{
}

static double fpTime()
{
	//time_t systime=time(0);
	clock_t sysClock = clock();
	return (double)sysClock/(double)CLOCKS_PER_SEC*1000.0;
}

/*
 * Start the timer (resetting to zero first)
 */
void Timer::Start()
{
    lastTime = fpTime();
    nbSteps = 0;
}

/*
 * Note the timing of the specified step
 */
void Timer::Step(const char *msg)
{
    static const char *overloadMsg = "Too many steps...cumulated";
    float newTime = fpTime();

    if (nbSteps == MAX_NB_STEPS) {
	times[nbSteps] += newTime-lastTime;
	steps[nbSteps] = overloadMsg;
    }
    else {
	times[nbSteps] = newTime-lastTime;
	steps[nbSteps] = msg;
	nbSteps++;
    }
    lastTime = newTime;
}

/*
 * Report the timing session
 */
float Timer::Report(const char *title)
{
    char report[1024];
    float cumul=0.0;
    int i;

    sprintf(report, "%s:\n", title);
    for (i=0; i<nbSteps; i++) {
	sprintf(report, "%s\t%-5.2f -> %s\n", report,
	    times[i], steps[i]);
        cumul += times[i];
    }
    sprintf(report,"%s\t%-5.2f -> TOTAL\n",report,  cumul);
    if (cumul >= threshold)
		std::cout <<report;
    return(cumul);
}

/*********************** End Of Timer.cc ***************************/
