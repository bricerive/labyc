// File Name: MacConsole.cpp
#ifdef LABYC_USE_SDL

#include "labyConfigAccess.h"
#include "MacConsole_SDL.h"

#define DEFAULT_PALETTE_FILE "laby.pal"
#define PALETTE_ENTRIES 64

#define kObjectWindowKind 2000 // Our window signature

bool MacConsole::Update()
{
    FrameBuff *fb = GetFrameBuff();
    unsigned short width = fb->Width();
    unsigned short height = fb->Height();
    Uint32 rmask = 0x000000ff;
    Uint32 gmask = 0x0000ff00;
    Uint32 bmask = 0x00ff0000;
    Uint32 amask = 0xff000000;
    int depth=32, pitch=4*width;
    SDL_Surface *surf = SDL_CreateRGBSurfaceFrom((void*)fb->PixP(), width, height, depth, pitch,rmask, gmask, bmask, amask);
    
    SDL_Texture *bitmapTex = SDL_CreateTextureFromSurface(renderer, surf);
    SDL_FreeSurface(surf);
    SDL_RenderClear(renderer);
    SDL_RenderCopy(renderer, bitmapTex, NULL, NULL);
    SDL_RenderPresent(renderer);
    SDL_DestroyTexture(bitmapTex);
    return false;
}

#define ADD_SPECIAL(i,s) if ((i)->n_special<MAX_SPECIAL_INTENTIONS) \
(i)->special[(i)->n_special++] = (s);

bool MacConsole::ReadInput(Intent &intent)
{
    CONFIG_VAR(double, Speed);
    CONFIG_VAR(double, RunFactor);
    
    static bool rotating_cw = false;
    static bool rotating_ccw = false;
    static bool moving_forward = false;
    static bool moving_backward = false;
    static bool running = false;
    static bool strafing = false;
    intent.force_x = intent.force_y = 0;
    intent.force_rotate = 0;
    intent.n_special = 0;
    
    // Handle events
    SDL_Event event;
    while( SDL_PollEvent(&event))
    {
        switch (event.type)
        {
            case SDL_KEYDOWN:
                switch (event.key.keysym.sym)
            {
                case SDLK_SPACE:
                    ADD_SPECIAL(&intent, INTENT_HALT);
                    break;
                case SDLK_RETURN:
                    ADD_SPECIAL(&intent, INTENT_LOCK);
                    break;
                case SDLK_q:
                    ADD_SPECIAL(&intent, INTENT_END_GAME);
                    break;
                case SDLK_LEFT:
                    rotating_ccw = true;
                    break;
                case SDLK_RIGHT:
                    rotating_cw = true;
                    break;
                case SDLK_UP:
                    moving_forward = true;
                    break;
                case SDLK_DOWN:
                    moving_backward = true;
                    break;
                case SDLK_LSHIFT: running = true; break;
                case SDLK_LALT: strafing = true; break;
                default:break;
            }
                break;
            case SDL_KEYUP:
                switch (event.key.keysym.sym) {
                    case SDLK_LEFT:
                        rotating_ccw = false;
                        break;
                    case SDLK_RIGHT:
                        rotating_cw = false;
                        break;
                    case SDLK_UP:
                        moving_forward = false;
                        break;
                    case SDLK_DOWN:
                        moving_backward = false;
                        break;
                    case SDLK_LSHIFT: running = false; break;
                    case SDLK_LALT: strafing = false; break;
                    default:break;
                }
                break;
            default:
                break;
        }
    }
    if (rotating_cw) {
        if (strafing)
            intent.force_y = -Speed;
		else
		    intent.force_rotate = -Speed;
    }
    if (rotating_ccw) {
        if (strafing)
            intent.force_y = Speed;
		else
		    intent.force_rotate = Speed;
    }
    if (moving_forward)
        intent.force_x = Speed;
    if (moving_backward)
        intent.force_x = -Speed;
    if (running) {
        intent.force_x = intent.force_x*RunFactor;
        intent.force_y = intent.force_y*RunFactor;
        intent.force_rotate = intent.force_rotate*RunFactor;
    }
    return false;
}

MacConsole::MacConsole()
: width(1280), height(960)
{
    //Initialize all SDL subsystems
    ASSERT_THROW(!SDL_Init(SDL_INIT_EVERYTHING));
    ASSERT_THROW(win = SDL_CreateWindow("Laby", 100, 100, width, height, SDL_WINDOW_SHOWN|SDL_WINDOW_RESIZABLE));
    renderer = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED);


}

MacConsole::~MacConsole()
{
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(win);

    SDL_Quit();}

/*********************** End Of MacConsole.cpp ***************************/
#endif
