// MacConsole.h - Mac version of the console
#pragma once

#ifdef LABYC_USE_SDL
#include <stdio.h>
#include "Console.h"
#include <SDL2/SDL.h>

class MacConsole : public Console {
 public:
    MacConsole();
    virtual ~MacConsole();
    virtual bool Update();
    virtual bool ReadInput(Intent &intent);
 private:
    int width, height;
    SDL_Window *win;
    SDL_Renderer *renderer;
};
#endif
