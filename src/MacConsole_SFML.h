// MacConsole.h - Mac version of the console
#pragma once

#ifndef LABYC_USE_SDL
#include <stdio.h>
#include "Console.h"
#include <SFML/Graphics.hpp>

class MacConsole : public Console {
 public:
    MacConsole();
    virtual ~MacConsole();
    virtual bool Update();
    virtual bool ReadInput(Intent &intent);
 private:
    int width, height;
    sf::RenderWindow mainWindow;
};
#endif
