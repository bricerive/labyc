// File Name: Console.cc
#include "Console.h"
#include "labyConfigAccess.h"
#include "mylib/config.h"

Console::Console()
	:frameBuff(0)
{
    CONFIG_VAR(int, FrameWidth);
    CONFIG_VAR(int, FrameHeight);
    force_x = 0;
    force_y = 0;
    force_rotate = 0;
    frameBuff = new FrameBuff(FrameHeight, FrameWidth);
}

Console::~Console()
{
	delete frameBuff;
}

FrameBuff *Console::GetFrameBuff()const
{
	if (frameBuff==0) {
		
	}
	return frameBuff;
}

/*********************** End Of Console.cc ***************************/
