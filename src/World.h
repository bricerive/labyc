// World.h -- interface for World class
#ifndef World_h
#define World_h

struct FrontWall {
    class Wall *wall;
    double z;
};

#include <vector>
using std::vector;
#include "Vertex.h"
#include "Wall.h"
#include "Texture.h"
#include "FrameBuff.h"
#include "View.h"

typedef unsigned int ScaleData;
typedef ScaleData *ScaleVec;

class World {
 public:
    World();
    ~World();
    void LoadFromFile(const char *dirName, const char *fileName);
    void Render(View *view, FrameBuff *fb);
    void ChangeWorld(View *view);
 private:
	struct Segment {
	    PIX *texP;
	    ScaleVec scVec;
	};
 	vector<Vertex *> vertices;
 	vector<Wall *> walls;
 	vector<Texture *> textures;
    FrontWall *frontWalls;
    ScaleVec *scaleVectors;
    void ComputeScales();
    void Transform(View *view);
    void Clip(View *view);
    void Draw(View *view, FrameBuff *frameBuff);
	void Slice32(FrameBuff *fb, Segment *seg, int col);
	void MakeWall(Vertex *v0, Vertex *v1, char id);
};

#endif
/*************************** End World.h ************************/
