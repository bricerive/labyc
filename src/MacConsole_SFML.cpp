// File Name: MacConsole.cpp
#ifndef LABYC_USE_SDL

#include "labyConfigAccess.h"
#include "MacConsole_SFML.h"

#define DEFAULT_PALETTE_FILE "laby.pal"
#define PALETTE_ENTRIES 64

#define kObjectWindowKind 2000 // Our window signature

bool MacConsole::Update()
{
    FrameBuff *fb = GetFrameBuff();
	unsigned short width = fb->Width();
	unsigned short height = fb->Height();
    
    mainWindow.clear();
    sf::Image image;
    image.create(width, height, (const unsigned char *)fb->PixP());
    sf::Texture texture;
    texture.loadFromImage(image);
    sf::Sprite sprite;
    sprite.setTexture(texture);
    mainWindow.draw(sprite);
    mainWindow.display();
    return false;
}

#define ADD_SPECIAL(i,s) if ((i)->n_special<MAX_SPECIAL_INTENTIONS) \
(i)->special[(i)->n_special++] = (s);

bool MacConsole::ReadInput(Intent &intent)
{
    CONFIG_VAR(double, Speed);
    CONFIG_VAR(double, RunFactor);
    
    static bool rotating_cw = false;
    static bool rotating_ccw = false;
    static bool moving_forward = false;
    static bool moving_backward = false;
    static bool running = false;
    static bool strafing = false;
    intent.force_x = intent.force_y = 0;
    intent.force_rotate = 0;
    intent.n_special = 0;
    
    // Handle events
    sf::Event event;
    if (mainWindow.pollEvent(event))
    {
        switch (event.type)
        {
            case sf::Event::KeyPressed:
                switch (event.key.code)
            {
                case sf::Keyboard::Key::Space:
                    ADD_SPECIAL(&intent, INTENT_HALT);
                    break;
                case sf::Keyboard::Key::Return:
                    ADD_SPECIAL(&intent, INTENT_LOCK);
                    break;
                case sf::Keyboard::Key::Q:
                    ADD_SPECIAL(&intent, INTENT_END_GAME);
                    break;
                case sf::Keyboard::Key::Left:
                    rotating_ccw = true;
                    break;
                case sf::Keyboard::Key::Right:
                    rotating_cw = true;
                    break;
                case sf::Keyboard::Key::Up:
                    moving_forward = true;
                    break;
                case sf::Keyboard::Key::Down:
                    moving_backward = true;
                    break;
                case sf::Keyboard::Key::LShift: running = true; break;
                case sf::Keyboard::Key::LAlt: strafing = true; break;
                default:break;
            }
                break;
            case sf::Event::KeyReleased:
                switch (event.key.code) {
                    case sf::Keyboard::Key::Left:
                        rotating_ccw = false;
                        break;
                    case sf::Keyboard::Key::Right:
                        rotating_cw = false;
                        break;
                    case sf::Keyboard::Key::Up:
                        moving_forward = false;
                        break;
                    case sf::Keyboard::Key::Down:
                        moving_backward = false;
                        break;
                    case sf::Keyboard::Key::LShift: running = false; break;
                    case sf::Keyboard::Key::LAlt: strafing = false; break;
                    default:break;
                }
                break;
            case sf::Event::MouseButtonPressed:
                break;
            case sf::Event::Resized:
                break;
                
            default:
                break;
        }
    }
    if (rotating_cw) {
        if (strafing)
            intent.force_y = -Speed;
		else
		    intent.force_rotate = -Speed;
    }
    if (rotating_ccw) {
        if (strafing)
            intent.force_y = Speed;
		else
		    intent.force_rotate = Speed;
    }
    if (moving_forward)
        intent.force_x = Speed;
    if (moving_backward)
        intent.force_x = -Speed;
    if (running) {
        intent.force_x = intent.force_x*RunFactor;
        intent.force_y = intent.force_y*RunFactor;
        intent.force_rotate = intent.force_rotate*RunFactor;
    }
    return false;
}

MacConsole::MacConsole()
: width(1280), height(960)
, mainWindow(sf::VideoMode(width, height, 32), "Laby", sf::Style::Resize | sf::Style::Close)
{
}

MacConsole::~MacConsole()
{
}

/*********************** End Of MacConsole.cpp ***************************/
#endif
