// View.h -- interface for View class
#ifndef View_h
#define View_h

#include "Console.h"
class World;

class View {
 public:
    View();
    virtual ~View();
    bool ChangeView(Intent &intent, World *world);
    void SetPos(double x, double y) {this->x=x; this->y=y;}
    double EyeDistance() {return(eyeDistance);}
    double ViewPlaneSize() {return(viewPlaneSize);}
    double X() {return(x);}
    double Y() {return(y);}
    double PixDX() {return(pixDx);}
    double PixDY() {return(pixDy);}
    double Sin() {return(as);}
    double Cos() {return(ac);}
 private:
    double x;
    double y;
    double a;
    double dx;
    double dy;
    double da;
    double as;
    double ac;
    double viewPlaneSize;
    double eyeDistance;
    double pixDx;
    double pixDy;
};

#endif
/*************************** End View.h ************************/
