#ifndef labyConfigAccess_h
#define labyConfigAccess_h

#include "mylib/config.h"

#define CONFIG_VAR(t,n) CONFIG_PARAM(t,"Config",n)

//#ifdef CONFIG_MAIN
//#define SHARED
//#else
//#define SHARED extern
//#endif
//
//typedef struct {
//	int FrameWidth;
//	int FrameHeight; // multiple of 8 for loop unrolling in Slice32
//	int TextureDim;
//	float ViewPlaneSize;
//	float ViewEyeDist;
//	float Speed;
//	float RunFactor;
//	int Inertia;
//	int LockSpeed;
//	float WallHeight;
//} labyConfigData;
//
//SHARED labyConfigData gConfig;

#endif
