// labyMain.cc
#define CONFIG_MAIN
#include "labyConfigAccess.h"
#include "Laby.h"
#include "MacConsole_SFML.h"
#include "MacConsole_SDL.h"
#include "mylib/error.h"
#include "mylib/log.h"
#include "mylib/config.h"
#include "mylib/configproviderptree.h"
#include <string>
#include <boost/filesystem.hpp>

using namespace std;
using namespace mylib;
using namespace boost::filesystem;

int main (int argc, char **argv)
{
    path mypath = system_complete( path( argv[0] ) ).parent_path();
    string exeName = strrchr(argv[0], '/')+1;
    
    boost::filesystem::path cwd( boost::filesystem::current_path() );
    boost::filesystem::current_path(mypath / ".." / "Resources");
    boost::filesystem::path cwd2( boost::filesystem::current_path() );

    // Setup config
    path configFilePath = cwd2/(exeName + ".config");
    config::TheProvider::AddProvider(new config::ProviderXml<>(configFilePath.string().c_str()));
    
    // Setup logging
    path logFilePath = mypath / (exeName + "_log.txt");
    std::ofstream logFile(logFilePath.string().c_str());
    Log::AddStreamBuf(logFile.rdbuf());
    Log::SetLevel(config::ConfigParam<Log::LogType>("Config", "logLevel"));
    
    LogF(LOG_TRACE, "################################################");
    LogF(LOG_TRACE, "###        NEW SESSION                       ###");
    LogF(LOG_TRACE, "################################################");
    LogF(LOG_TRACE, "Entering main");
    LogF(LOG_TRACE, "mypath: %s", mypath.string().c_str());
    LogF(LOG_TRACE, "Config file: %s", configFilePath.string().c_str());
    LogF(LOG_TRACE, "Log file: %s", logFilePath.string().c_str());
    LogF(LOG_TRACE, "cwd: %s", cwd2.string().c_str());


    MacConsole *console = new MacConsole();
    Laby *laby = new Laby(console);
    
    laby->Run();
    
    delete laby;
    delete console;
    return 0;
}
