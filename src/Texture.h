/*
 * Texture.h -- interface for Texture class
 */

#ifndef Texture_h
#define Texture_h

#include "FrameBuff.h"

#define FLAG_TEXTURE_LAST FLAG_GENERIC_LAST
class Texture {
 public:
    Texture();
    virtual ~Texture();
    bool Load(const char *fileName);
    bool Generate(int code);
    PIX *Point(double t);
 private:
    int dim;
    PIX *texP;
    bool LoadIcon(char *fileBuff, int fileSize);
	PIX &TexP(int r, int c) {return *(texP + dim * r + c);}
	void FillLast();
};
#endif
/*************************** End Texture.h ************************/
