// File Name: World.cpp
#include <stdio.h>
#include <iostream>
#include <string.h>
#include "World.h"
#include "labyConfigAccess.h"
#include "mylib/error.h"
#include <boost/filesystem.hpp>
#include <dirent.h>
#include <algorithm>

using namespace boost::filesystem;
using namespace mylib;
using namespace std;

typedef enum {
    WALL_NORMAL,
    WALL_ANIM=23,
    WALL_SLIDING=24,
    WALL_ROTATING=25
} WALL_TYPE;

static PIX nullTex = 0x80C04040; // ABGR

#define VECT_NB (int)(WallHeight*FrameHeight/ViewPlaneSize)

#define PREMULT_VECT 1

void World::Slice32(FrameBuff *fb, Segment *seg, int col)
{
    CONFIG_VAR(int, FrameWidth);
    CONFIG_VAR(int, FrameHeight);
#if PREMULT_VECT
    int width = FrameWidth;
    PIX *fpt = fb->PixP()+col;
    PIX *fpte = fpt+FrameHeight*width;
    PIX *texP = seg->texP;
    ScaleVec scVec = seg->scVec;

    while (fpt < fpte) {
		*fpt = *((PIX *)((unsigned char *)texP + *scVec));
		fpt += width;
		*fpt = *((PIX *)((unsigned char *)texP + *(scVec+1)));
		fpt += width;
		*fpt = *((PIX *)((unsigned char *)texP + *(scVec+2)));
		fpt += width;
		*fpt = *((PIX *)((unsigned char *)texP + *(scVec+3)));
		fpt += width;
		*fpt = *((PIX *)((unsigned char *)texP + *(scVec+4)));
		fpt += width;
		*fpt = *((PIX *)((unsigned char *)texP + *(scVec+5)));
		fpt += width;
		*fpt = *((PIX *)((unsigned char *)texP + *(scVec+6)));
		fpt += width;
		*fpt = *((PIX *)((unsigned char *)texP + *(scVec+7)));
		scVec+=8;
		fpt += width;
    }
#else
    int width = FrameWidth;
    PIX *fpt = fb->PixP()+col;
    PIX *fpte = fpt+FrameHeight*width;
    PIX *texP = seg->texP;
    ScaleVec scVec = seg->scVec;

    while (fpt < fpte) {
		*fpt = *(texP + *scVec);
		scVec++;
		fpt += width;
    }
#endif
}

void World::ComputeScales()
{
    CONFIG_VAR(int, TextureDim);
    CONFIG_VAR(int, FrameHeight);
    CONFIG_VAR(int, WallHeight);
    CONFIG_VAR(int, ViewPlaneSize);
    
    int i,j;
    double delta, sum;
    int nbRows, start, row;
    int tDim = TextureDim;

    // Do only even sizes
    for (i=0; i<VECT_NB; i+=2) {
		nbRows = FrameHeight;
		scaleVectors[i/2] = new ScaleData[nbRows];
		start = (nbRows - i)>>1;
		delta = (double)(tDim-1)/(double)i;
		sum = max(0.0, (double)((i-nbRows)>>1)*delta);

#if PREMULT_VECT
		for (j=0; j<start; j++) {
		    *(*(scaleVectors+(i>>1)) + j) = tDim*tDim*4; //PREMULT
		    *(*(scaleVectors+(i>>1)) + (nbRows-1-j)) = tDim*tDim*4; //PREMULT
		}	    
		for (; j<nbRows/2; j++) {
		    row = (int)(sum+.5);
		    *(*(scaleVectors + (i>>1)) + j) = row*tDim*4; //PREMULT
		    *(*(scaleVectors + (i>>1)) + (nbRows-1-j)) = (tDim-1-row)*tDim*4; //PREMULT
		    sum += delta;
		}
#else
		for (j=0; j<start; j++) {
		    *(*(scaleVectors+(i>>1)) + j) = tDim*tDim;
		    *(*(scaleVectors+(i>>1)) + (nbRows-1-j)) = tDim*tDim;
		}	    
		for (; j<nbRows/2; j++) {
		    row = (int)(sum+.5);
		    *(*(scaleVectors + (i>>1)) + j) = row*tDim;
		    *(*(scaleVectors + (i>>1)) + (nbRows-1-j)) = (tDim-1-row)*tDim;
		    sum += delta;
		}
#endif
    }
}

void World::Transform(View *view)
{
    for (vector<Vertex *>::iterator vertexI=vertices.begin(); vertexI!=vertices.end(); vertexI++)
    	(*vertexI)->Transform(view);    // Transform each vertex
}

void World::Clip(View *view)
{
	// Clip each wall
	for (vector<Wall *>::iterator wallI=walls.begin(); wallI!=walls.end(); wallI++)
		(*wallI)->ClipAndSort(view, frontWalls);
}

void World::Draw(View *view, FrameBuff *fb)
{
    CONFIG_VAR(int, FrameWidth);
    CONFIG_VAR(int, WallHeight);
    CONFIG_VAR(int, TextureDim);

    int col;
    Segment seg;
    int wSize;
    double pDy, Vx, Vy, dVy;

    pDy = view->PixDY();
    Vx = view->EyeDistance();
    Vy = view->ViewPlaneSize();
    dVy = 2*view->ViewPlaneSize()/FrameWidth;

    // Draw each column of the frame buffer
    for (col=0; col<FrameWidth; col++) {
		
		if (frontWalls[col].wall) {
		    // Compute size of the wall
		    wSize = (int)(WallHeight*frontWalls[col].z/pDy + .5);
		    
		    // Shrink to even resolution for symetric display
		    wSize &= 0xFFFFFFFE;

		    seg.texP = frontWalls[col].wall->RayIntersect(Vx, Vy);
		    seg.scVec = scaleVectors[wSize/2];
		}
		else {
		    seg.texP = &nullTex - TextureDim * TextureDim;
		    seg.scVec = scaleVectors[0];
		}
		Vy-=dVy;

		// Draw the pixels
        Slice32(fb, &seg, col);
    }
}

void World::MakeWall(Vertex *v0, Vertex *v1, char id)
{
	Wall *w0, *w1;
	static int animBaseId=52;
	
	switch(id) {
	case WALL_SLIDING:
		w0 = new SlidingWall(v0,v1,textures[id]);
		w1 = new Wall(v1,v0,textures[id+26]);
		break;
	case WALL_ANIM:
		w0 = new AnimWall(v0,v1,textures[animBaseId]);
        animBaseId = min<int>(textures.size()-1, animBaseId+1);
		w1 = new AnimWall(v1,v0,textures[animBaseId]);
        animBaseId = min<int>(textures.size()-1, animBaseId+1);
		break;
	case WALL_ROTATING:
		w0 = new RotatingWall(v0,v1,textures[id]);
		w1 = new RotatingWall(v1,v0,textures[id+26]);
		break;
	default:
		w0 = new Wall(v0,v1,textures[id]);
		w1 = new Wall(v1,v0,textures[id+26]);
		break;
	}
	walls.push_back(w0);
	walls.push_back(w1);
}

static void LoadFile(string &filePath, vector<string> &lines)
{
    FILE *fd;
    fd = fopen(filePath.c_str(), "rt");
    ASSERT_THROW(fd)("Cannot open world file %s", filePath.c_str());
    char buf[128];
    while (fgets(buf,128,fd)) {
        buf[strlen(buf)-1]=0; // remove \n
        lines.push_back(buf);
    }
    fclose(fd);
}

void World::LoadFromFile(const char *dirName, const char *fileName)
{
    std::string filePath(dirName);
    filePath+="/";
    filePath+=fileName;

    vector<string> lines;
    LoadFile(filePath, lines);

    // Create textures
    const int nbTexturesWanted=54; // 26*2+2
    directory_iterator itr("textures");
    directory_iterator dirEnd;
    for (int i=0; textures.size()<nbTexturesWanted; )
    {
        if (itr==dirEnd) {
            Texture *tex = new Texture;
            tex->Generate(i++);
            textures.push_back(tex);
        } else {
            Texture *tex = new Texture;
            if (!tex->Load(itr->path().c_str()))
                textures.push_back(tex);
            else
                delete tex;
            ++itr;
        }
    }

    // Create the layout
    vector<string> layout(lines.begin(), lines.end());
    ASSERT_THROW(layout.size() & 1)("Even number of rows");
    int nrows = layout.size()/2;
    int ncols=0;
    for (string &s: layout) {
        ASSERT_THROW(s.size() & 1)("Even row");
        ncols = max<int>(ncols, s.size()/2);
    }
    for (string &s: layout)
        s.resize(ncols*2+1,' ');
    
    /* Create lattice */
    for (int row=0; row<=nrows; row++)
		for (int col=0; col<=ncols; col++)
		    vertices.push_back(new Vertex(10*col, 10*row));

    string s;
    int nVerticePerRow = ncols+1;
    for (int row=0; row<nrows; row++) {
        s = layout[row*2];
		for (int col=0; col<ncols; col++) {
		    if (s[col*2+1]!=' ')
		    	MakeWall(*(vertices.begin()+nVerticePerRow*row+col),*(vertices.begin()+nVerticePerRow*row+col+1),s[col*2+1]-'a');
		}
        s = layout[row*2+1];
		for (int col=0; col<ncols+1; col++) {
		    if (s[col*2]!=' ')
		    	MakeWall(*(vertices.begin()+nVerticePerRow*row+col),*(vertices.begin()+nVerticePerRow*(row+1)+col),s[col*2]-'a');
		}
    }
    s = layout[nrows*2];
    for (int col=0; col<ncols; col++) {
		if (s[col*2+1]!=' ')
	    	MakeWall(*(vertices.begin()+nVerticePerRow*nrows+col),*(vertices.begin()+nVerticePerRow*nrows+col+1),s[col*2+1]-'a');
    }
}

// Renders the world seen from view into framebuff
void World::Render(View *view, FrameBuff *fb)
{
    CONFIG_VAR(int, FrameWidth);
    // initialize the z buffer
    for (int col=0; col<FrameWidth; col++) {
		frontWalls[col].z = -1000000000.0;
		frontWalls[col].wall = NULL;
    }

    Transform(view);
    Clip(view);
    Draw(view, fb);
}

/*
 * Update the world contents
 */
void World::ChangeWorld(View *view)
{
    // Animate each wall
	for (vector<Wall *>::iterator wallI=walls.begin(); wallI!=walls.end(); wallI++)
		(*wallI)->Animate(view);
}

World::~World()
{
    CONFIG_VAR(int, FrameHeight);
    CONFIG_VAR(int, WallHeight);
    CONFIG_VAR(int, ViewPlaneSize);

    delete frontWalls;
    for (int i=0; i<VECT_NB; i+=2)
		delete scaleVectors[i/2];
    delete scaleVectors;
    for (vector<Vertex *>::iterator vertexI=vertices.begin(); vertexI!=vertices.end(); vertexI++)
    	delete *vertexI;    // Transform each vertex
    for (vector<Texture *>::iterator textureI=textures.begin(); textureI!=textures.end(); textureI++)
    	delete *textureI;    // Transform each vertex
    for (vector<Wall *>::iterator wallI=walls.begin(); wallI!=walls.end(); wallI++)
    	delete *wallI;    // Transform each vertex
}

World::World()
{
    CONFIG_VAR(int, FrameHeight);
    CONFIG_VAR(int, WallHeight);
    CONFIG_VAR(int, ViewPlaneSize);
    CONFIG_VAR(int, FrameWidth);
    frontWalls = new FrontWall[FrameWidth];
    scaleVectors = new ScaleVec[VECT_NB];
    ComputeScales();
}

/*********************** End Of World.c ***************************/

