// File Name: Laby.cc
#include "Laby.h"
#include "labyConfigAccess.h"
#include "mylib/config.h"
#include <string>

using namespace std;

Laby::Laby(Console *console_)
{
    world = new World;
    console = console_;
    frameBuff = console->GetFrameBuff();
    view = new View;
    timer = new Timer;
}

Laby::~Laby()
{
    delete world;
    delete view;
    delete timer;
}

void Laby::Run()
{
    bool done=false;
    int cnt=0;

    timer->Start();

    static const unsigned int maxPath=1024;
    char cwd[maxPath];
    getcwd(cwd, maxPath);
    CONFIG_VAR(string, labyrinthFile);
    world->LoadFromFile(cwd, labyrinthFile.c_str());
    view->SetPos(-5,-5);
    while (!done) {
		world->Render(view, frameBuff);
		console->Update();
        Intent intent;
		console->ReadInput(intent);
		view->ChangeView(intent, world);
		world->ChangeWorld(view);
		done = IsDone(intent);
		if (++cnt==1000) {
		    timer->Report("1000 frames");
		    cnt=0;
		    timer->Start();
		}
    }
}

int Laby::IsDone(Intent &intent)
{
    for (int i=0; i<intent.n_special; i++)
        while (intent.n_special--)
            if (intent.special[i] == INTENT_END_GAME)
                return true;
    return false;
}

/*********************** End Of Laby.cc ***************************/
