/*
 * xgifload.c  -  based strongly on...
 *
 * gif2ras.c - Converts from a Compuserve GIF (tm) image to a Sun Raster image.
 *
 * Copyright (c) 1988, 1989 by Patrick J. Naughton
 *
 * Author: Patrick J. Naughton
 * naughton@wind.sun.com
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose and without fee is hereby granted,
 * provided that the above copyright notice appear in all copies and that
 * both that copyright notice and this permission notice appear in
 * supporting documentation.
 *
 * This file is provided AS IS with no warranties of any kind.  The author
 * shall have no liability with respect to the infringement of copyrights,
 * trade secrets or any patents by this file or any part thereof.  In no
 * event will the author be liable for any lost revenue or profits or
 * other special, indirect and consequential damages.
 *
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

typedef int boolean;
typedef unsigned char byte;

#define MIN(a,b) (((a)>(b))? (b): (b))

#define IMAGESEP      0x2c
#define INTERLACEMASK 0x40
#define COLORMAPMASK  0x80

int BitOffset = 0,		/* Bit Offset of next code */
XC = 0, YC = 0,		/* Output X and Y coords of current pixel */
Pass = 0,			/* Used by output routine if interlaced pic */
OutCount = 0,		/* Decompressor output 'stack count' */
Width, Height,		/* image dimensions */
BytesPerScanline,		/* bytes per scanline in output raster */
CodeSize,			/* Code size, read from GIF header */
InitCodeSize,		/* Starting code size, used during Clear */
Code,			/* Value returned by ReadCode */
MaxCode,			/* limiting value for current code size */
ClearCode,			/* GIF clear code */
EOFCode,			/* GIF end-of-information code */
CurCode, OldCode, InCode,	/* Decompressor variables */
FirstFree,			/* First free code, generated per GIF spec */
FreeCode,			/* Decompressor, next free slot in hash table*/
FinChar,			/* Decompressor variable */
BitMask,			/* AND mask for data size */
ReadMask;			/* Code AND mask for current code size */

#define True     1
#define False    0

boolean Interlace;

byte *Image;			/* The result array */
byte *Raster;			/* The raster data stream, unblocked */

/* An output array used by the decompressor */
int OutCode[1025];

int   ReadCode( void );
int   log2( int );
void  AddToPixel( byte );

bool loadGIF(int dim, unsigned char *texP, unsigned char *fileBuff, int fileSize)
{
    byte *ptr = fileBuff;
    byte ch;
    int i,j;
    
    /* The hash table used by the decompressor */
    int Prefix[4096];
    int Suffix[4096];
    /* The color map, read from the GIF header */
    byte Red[256], Green[256], Blue[256];
    
    unsigned  char ch1;
    byte *ptr1;
    
    if (!(Raster = (byte *) malloc(fileSize))) {
        free( ptr );
        //printf("not enough memory to read gif file");
        goto Error;
    }
    
    BitOffset = 0;
    XC = 0, YC = 0;
    Pass = 0;
    OutCount = 0;
    
    ptr += 6; /* Skip file type mark */
    ptr += 4; /* Skip screen descriptor */
    
    /* Get colormap info */
    {
        int colorMapSize;
        
        ch = *(ptr++);
        colorMapSize = 1 << ((ch & 7) + 1);
        BitMask = colorMapSize - 1;
        ptr++; /* Skip background color */
        if (*(ptr++)) {		/* supposed to be NULL */
            //printf("corrupt GIF file (bad screen descriptor)");
            goto Error;
        }
        if (ch & COLORMAPMASK)
            for (i = 0; i < colorMapSize; i++) {
                Red[i]   = *(ptr++);
                Green[i] = *(ptr++);
                Blue[i]  = *(ptr++);
            }
    }
    
    /* Check for image seperator */
    if (*(ptr++) != IMAGESEP) {
        //printf("corrupt GIF file (no image separator)");
        goto Error;
    }
    
    /* Now read in values from the image descriptor */
    ptr += 4; /* Skip image offsets */
    ch        = *(ptr++);
    Width     = ch + 0x100 * *(ptr++);
    ch        = *(ptr++);
    Height    = ch + 0x100 * *(ptr++);
    Interlace = ((*(ptr++) & INTERLACEMASK) ? True : False);
    
    /* Create the image */
    if (!(Image = (unsigned char *)malloc(Width * Height))) goto Error;
    
    /* Start reading the raster data. First we get the intial code size
     * and compute decompressor constant values, based on this code size.
     */
    CodeSize  = *(ptr++);
    ClearCode = (1 << CodeSize);
    EOFCode   = ClearCode + 1;
    FreeCode  = FirstFree = ClearCode + 2;
    
    /* The GIF spec has it that the code size is the code size used to
     * compute the above values is the code size given in the file, but the
     * code size used in compression/decompression is the code size given in
     * the file plus one. (thus the ++).
     */
    CodeSize++;
    InitCodeSize = CodeSize;
    MaxCode      = (1 << CodeSize);
    ReadMask     = MaxCode - 1;
    
    /* Read the raster data.  Here we just transpose it from the GIF array
     * to the Raster array, turning it from a series of blocks into one long
     * data stream, which makes life much easier for ReadCode().
     */
    ptr1 = Raster;
    do {
        ch = ch1 = *(ptr++);
        while (ch--) *ptr1++ = *(ptr++);
        if ((ptr1 - Raster) > fileSize) {
            //printf("corrupt GIF file (unblock)");
            goto Error;
        }
    } while(ch1);
    
    BytesPerScanline    = Width;
    
    /* Decompress the file, continuing until you see the GIF EOF code.
     * One obvious enhancement is to add checking for corrupt files here.
     */
    Code = ReadCode();
    while (Code != EOFCode) {
        
        /* Clear code sets everything back to its initial value, then reads the
         * immediately subsequent code as uncompressed data.
         */
        if (Code == ClearCode) {
            CodeSize = InitCodeSize;
            MaxCode  = (1 << CodeSize);
            ReadMask = MaxCode - 1;
            FreeCode = FirstFree;
            CurCode  = OldCode = Code = ReadCode();
            FinChar  = CurCode & BitMask;
            AddToPixel(FinChar);
        }
        else {
            /* If not a clear code, then must be data: save same as CurCode
             * and InCode
             */
            CurCode = InCode = Code;
            
            /* If greater or equal to FreeCode, not in the hash table yet;
             * repeat the last character decoded
             */
            if (CurCode >= FreeCode) {
                CurCode = OldCode;
                OutCode[OutCount++] = FinChar;
            }
            
            /* Unless this code is raw data, pursue the chain pointed to by
             * CurCode  through the hash table to its end; each code in the
             * chain puts its associated output code on the output queue.
             */
            while (CurCode > BitMask) {
                if (OutCount > 1024) {
                    //fprintf(stderr,"\nCorrupt GIF file (OutCount)!\n");
                    goto Error;
                }
                OutCode[OutCount++] = Suffix[CurCode];
                CurCode = Prefix[CurCode];
            }
            
            /* The last code in the chain is treated as raw data. */
            FinChar             = CurCode & BitMask;
            OutCode[OutCount++] = FinChar;
            
            /* Now we put the data out to the Output routine.
             * It's been stacked LIFO, so deal with it that way...
             */
            for (i = OutCount - 1; i >= 0; i--)
                AddToPixel(OutCode[i]);
            OutCount = 0;
            
            /* Build the hash table on-the-fly. No table is stored in the
             * file.
             */
            Prefix[FreeCode] = OldCode;
            Suffix[FreeCode] = FinChar;
            OldCode          = InCode;
            
            /* Point to the next slot in the table.  If we exceed the current
             * MaxCode value, increment the code size unless it's already 12.
             * If it is, do nothing: the next code decompressed better be CLEAR
             */
            FreeCode++;
            if (FreeCode >= MaxCode) {
                if (CodeSize < 12) {
                    CodeSize++;
                    MaxCode *= 2;
                    ReadMask = (1 << CodeSize) - 1;
                }
            }
        }
        Code = ReadCode();
    }
    free(Raster);
    
    
	unsigned char *src,*dst;
	int scale;
	int k,l;
	int sumR, sumG, sumB;
    
	//fprintf(stderr, "Wrong texture dim (%dx%d)... clipping\n",  Height, Width);
    
	scale = MIN(Width/dim, Height/dim);
	// For walls, scaled
	for (i=0; i<dim; i++) {
	    src = Image + Width*(Height/2 - dim/2*scale + i*scale) + Width/2 - dim/2*scale;
	    for (j=0; j<dim; j++) {
			sumR=sumG=sumB=0;
			for (k=0; k<scale; k++)
			    for (l=0; l<scale; l++) {
			    	sumR += Red[*(src+l+k*Width)];
			    	sumG += Green[*(src+l+k*Width)];
			    	sumB += Blue[*(src+l+k*Width)];
				}
			dst = (unsigned char *)(texP + dim * i + j);
 			dst[0] = sumR / (scale*scale);
 			dst[1] = sumG / (scale*scale);
 			dst[2] = sumB / (scale*scale);
 			dst[3] = 255;
 			src += scale;
	    }
	}
    
    free(Image);
    
    return(0);
Error:
    return(-1);
}


/* Fetch the next code from the raster data stream.  The codes can be
 * any length from 3 to 12 bits, packed into 8-bit bytes, so we have to
 * maintain our location in the Raster array as a BIT Offset.  We compute
 * the byte Offset into the raster array by dividing this by 8, pick up
 * three bytes, compute the bit Offset into our 24-bit chunk, shift to
 * bring the desired code to the bottom, then mask it off and return it. 
 */
int
ReadCode( void )
{
    int RawCode, ByteOffset;
    
    ByteOffset = BitOffset / 8;
    RawCode    = Raster[ByteOffset] + (0x100 * Raster[ByteOffset + 1]);
    
    if (CodeSize >= 8)
        RawCode += (0x10000 * Raster[ByteOffset + 2]);
    
    RawCode  >>= (BitOffset % 8);
    BitOffset += CodeSize;
    
    return(RawCode & ReadMask);
}

void
AddToPixel(byte Index)
{
    if (YC<Height)
        *(Image + YC * BytesPerScanline + XC) = Index;
    
    /* Update the X-coordinate, and if it overflows, update the Y-coordinate */
    
    if (++XC == Width) {
        
        /* If a non-interlaced picture, just increment YC to the next scan line. 
         * If it's interlaced, deal with the interlace as described in the GIF
         * spec.  Put the decoded scan line out to the screen if we haven't gone
         * past the bottom of it
         */
        
        XC = 0;
        if (!Interlace) YC++;
        else {
            switch (Pass) {
                case 0:
                    YC += 8;
                    if (YC >= Height) {
                        Pass++;
                        YC = 4;
                    }
                    break;
                case 1:
                    YC += 8;
                    if (YC >= Height) {
                        Pass++;
                        YC = 2;
                    }
                    break;
                case 2:
                    YC += 4;
                    if (YC >= Height) {
                        Pass++;
                        YC = 1;
                    }
                    break;
                case 3:
                    YC += 2;
                    break;
                default:
                    break;
            }
        }
    }
}
