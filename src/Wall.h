/*
 * Wall.h -- interface for Wall class
 */

#ifndef Wall_h
#define Wall_h

#include "Vertex.h"
#include "Texture.h"
#include "World.h"
#include "View.h"

class Wall {
 public:
    Wall(Vertex *v1, Vertex *v2, Texture *t);
    virtual ~Wall();
    void ClipAndSort(View *view, FrontWall *frontWalls);
    virtual PIX *RayIntersect(double Vx, double Vy);
    virtual void Animate(View *view);
 protected:
    double RayIntersectRatio(double Vx, double Vy);
 	double X1()const {return vertex1->X();}
 	double Y1()const {return vertex1->Y();}
 	double X2()const {return vertex2->X();}
 	double Y2()const {return vertex2->Y();}
 	void X1(double val) {vertex1->X(val);}
 	void Y1(double val) {vertex1->Y(val);}
 	void X2(double val) {vertex2->X(val);}
 	void Y2(double val) {vertex2->Y(val);}
    Vertex *vertex1;
    Vertex *vertex2;
    Texture *texture;
    //WALL_TYPE type;
};

class SlidingWall : public Wall {
 public:
	SlidingWall(Vertex *v1, Vertex *v2, Texture *t);
    void Animate(View *view);
 private:
	double dMax, dd, dxMax, dyMax;
};

class RotatingWall : public Wall {
 public:
	RotatingWall(Vertex *v1, Vertex *v2, Texture *t);
    void Animate(View *view);
};

class AnimWall : public Wall {
 public:
	AnimWall(Vertex *v1, Vertex *v2, Texture *t);
    virtual PIX *RayIntersect(double Vx, double Vy);
    void Animate(View *view);
 private:
    int toggle;
};

#endif
/*************************** End Wall.h ************************/
