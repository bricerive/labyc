// Vertex.h -- interface for Vertex class
#ifndef Vertex_h
#define Vertex_h

class View;

class Vertex {
 public:
    Vertex() {}
    Vertex(double x, double y) {SetPos(x,y);}
    virtual ~Vertex();
    void Transform(View *view);
    double X()const {return x;}
    double Y()const {return y;}
    double TX()const {return tx;}
    double TY()const {return ty;}
    double Proj()const {return proj;}
    void X(double val) {x=val;}
    void Y(double val) {y=val;}
    void SetPos(double x_, double y_) {x=x_; y=y_;}
    void SetTPos(double tx_, double ty_) {tx=tx_; ty=ty_;}
    void SetProj(double p) {proj=p;}
    void Rotate(Vertex *center, double a);
 private:
    double x;
    double y;
    double tx;
    double ty;
    double proj;
};
#endif
/*************************** End Vertex.h ************************/
