
# Welcome

This project is a playground to understand and experiment with Raycasting.
Raycasting is a technique for pseudo-3d rendering that was used mostly in the first first-person
shooter games (think Wolfenstein 3d).

You can see a quick demo [here](https://youtu.be/M-14lN4PlNU)