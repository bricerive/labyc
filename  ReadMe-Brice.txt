----------------------------------------------------------------
added a wiki and a video

The wiki info:

## Wiki features

This wiki uses the [Markdown](http://daringfireball.net/projects/markdown/) syntax. The [MarkDownDemo tutorial](https://bitbucket.org/tutorials/markdowndemo) shows how various elements are rendered. The [Bitbucket documentation](https://confluence.atlassian.com/x/FA4zDQ) has more information about using a wiki.

The wiki itself is actually a git repository, which means you can clone it, edit it locally/offline, add images or any other file type, and push it back to us. It will be live immediately.

Go ahead and try:

```
$ git clone https://bricerive@bitbucket.org/bricerive/labyc.git/wiki
```

Wiki pages are normal files, with the .md extension. You can edit them locally, as well as creating new ones.

## Syntax highlighting


You can also highlight snippets of text (we use the excellent [Pygments][] library).

[Pygments]: http://pygments.org/


Here's an example of some Python code:

```
#!python

def wiki_rocks(text):
    formatter = lambda t: "funky"+t
    return formatter(text)
```


You can check out the source of this page to see how that's done, and make sure to bookmark [the vast library of Pygment lexers][lexers], we accept the 'short name' or the 'mimetype' of anything in there.
[lexers]: http://pygments.org/docs/lexers/


Have fun!

----------------------------------------------------------------
181124- recap

I could build it with CLion (after fixing the dev lib stage path)
It builds under src/cmake-build-[debug|release]
I put that directory under .gitignore
----------------------------------------------------------------
ToDo:
Rid SFML from X11 dependency? Use SDL?
----------------------------------------------------------------
----------------------------------------------------------------
Started making a CMake based version
took src and rsrc from ToMoveToGit/labyC
will use platypusIt

Fails on Carbon stuff - because Carbon only builds in 32 bits
----------------------------------------------------------------
If I set it up for 32 bits, I get errors with all the other libs that are built for 64bits.
Is there another project that uses 32bits for Carbon and boost or mylib or others?
----------------------------------------------------------------
Two options:
Build dev libs in 32bit mode
Recode the application with SFML
----------------------------------------------------------------
Try with SFML - works
----------------------------------------------------------------
SFML requires X11 - Yosemite does not have be by default -> requires an install
----------------------------------------------------------------
